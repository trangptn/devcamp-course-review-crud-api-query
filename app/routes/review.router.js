const express = require("express");

const router = express.Router();

const {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getReviewByIDMiddleware,
    deleteReviewMiddleware,
    updateReviewMiddleware
} = require("../middlewares/review.middleware");

const reviewController=require('../controllers/review.controller');

router.get("/", getAllReviewMiddleware,reviewController.getAllReviews);

router.post("/", createReviewMiddleware, reviewController.createReview);

router.get("/:reviewId", getReviewByIDMiddleware,reviewController.getReviewById);

router.put("/:reviewId", updateReviewMiddleware, reviewController.updateReviewById);

router.delete("/:reviewId", deleteReviewMiddleware, reviewController.deleteReviewById);

module.exports = router;