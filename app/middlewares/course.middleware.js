const getAllCourseMiddleware = (req, res, next) => {
    console.log("GET all course middleware");

    next();
}

const getAllReviewOfCourseMiddleware = (req, res, next) => {
    console.log("GET all review of course middleware");

    next();
}

const createCourseMiddleware = (req, res, next) => {
    console.log("POST course middleware");

    next();
}

const getCourseByIDMiddleware = (req, res, next) => {
    console.log("GET course by id middleware");

    next();
}

const updateCourseMiddleware = (req, res, next) => {
    console.log("GET all course middleware");

    next();
}

const deleteCourseMiddleware = (req, res, next) => {
    console.log("GET all course middleware");

    next();
}

module.exports = {
    getAllCourseMiddleware,
    getAllReviewOfCourseMiddleware,
    createCourseMiddleware,
    getCourseByIDMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}