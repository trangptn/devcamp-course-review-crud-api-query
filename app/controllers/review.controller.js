const reviewModel = require("../models/review.model");
const courseModel = require("../models/course.model");


const mongoose = require("mongoose");
const createReview = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        starts,
        note,
        courseid
    } = req.body;

    // B2: Validate du lieu
    if( !mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "course Id is not Valid",
            "status":"Bad request"
        })
    }

    // start la so nguyen lon hon 0 va nho hon 6
    // Number.isInteger(starts) && starts >0 && starts <6
        if(starts=== undefined || !(Number.isInteger(starts) && starts >0 && starts <6))
        {
            return res.status(400).json({
                message: "Starts is not Valid",
                "status":"Bad request"
            })
        }
        // B3: Xu ly du lieu

        var newReview = {
            _id: new mongoose.Types.ObjectId(),
            starts: starts,
            note: note
        }
        try {
            const result = await reviewModel.create(newReview);

            const updateCourse = await courseModel.findByIdAndUpdate(courseid,{
                $push: { reviews : result._id}
                })
                return res.status(200).json({
                    message: "Create review successfully",
                    course: updateCourse,
                    data: result
                })
        } catch (error) {
            console.log(error);
            return res.status(500).json({
                status:" Internal sever error",
                message:error.message
            })
        }
    }
const getAllReviews = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await courseModel.find();

        return res.status(200).json({
            message: "Lay danh sach review thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getReviewById = async (req, res) => {
    // B1: Thu thap du lieu
    const reviewId = req.params.reviewId;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            message: "Review ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await reviewModel.findById(reviewId);

        if(result) {
            return res.status(200).json({
                message: "Lay thong tin review thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin review"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateReviewById = async (req, res) => {
    // B1: Thu thap du lieu
    const reviewId = req.params.reviewId;
    const {
        starts,
        note
    } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            message: "Review ID khong hop le"
        })
    }
    if(starts=== undefined || !(Number.isInteger(starts) && starts >0 && starts <6))
    {
        return res.status(400).json({
            message: "start is not Valid",
            "status":"Bad request"
        })
    }
    // B3: Xu ly du lieu
    try {
        var newUpdateReview = {};
        if(starts) {
            newUpdateReview.starts = starts
        }
        if(note) {
            newUpdateReview.note = note
        }

        const result = await reviewModel.findByIdAndUpdate(reviewId, newUpdateReview);

        if(result) {
            return res.status(200).json({
                message: "Update thong tin review thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}   

const deleteReviewById = async (req, res) => {
    // B1: Thu thap du lieu
    const reviewId = req.params.reviewId;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            message: "Review ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await reviewModel.findByIdAndRemove(reviewId);

       
        if(result) {
            return res.status(200).json({
                message: "Xoa thong tin review thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

module.exports = {
    createReview,
    getAllReviews,
    getReviewById,
    updateReviewById,
    deleteReviewById
}

