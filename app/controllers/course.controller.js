const courseModel = require("../models/course.model");
const mongoose = require("mongoose");

const createCourse = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;

    // console.log(req.body);
    // { reqTitle: 'R46', reqDescription: 'React & NodeJS', reqStudent: 20 }

    // B2: Validate du lieu
    if(!reqTitle) {
        return res.status(400).json({
            message: "Title khong hop le"
        })
    }

    if(reqStudent < 0) {
        return res.status(400).json({
            message: "So hoc sinh khong hop le"
        })
    }

    try {
        // B3: Xu ly du lieu

        var newCourse = {
            title: reqTitle,
            description: reqDescription,
            noStudent: reqStudent
        }

        const result = await courseModel.create(newCourse);

        return res.status(201).json({
            message: "Tao course thanh cong",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllCourses = async (req, res) => {
    // B1: Chuan bi du lieu
    const  courseName=req.query.courseName;
    const  minStudent = req.query.minStudent;
    const maxStudent = req.query.maxStudent;
    // Tao ra dieu kien loc
    let condition ={};
    if(courseName){
        condition.title = courseName;
    }

    if(minStudent)
    {
        condition.noStudent={$gte:minStudent};
    }
    if(maxStudent)
    {
        condition.noStudent={...condition.noStudent, $lte:maxStudent};
    }

    let condition2={
        title : courseName,
        $or : [
            {noStudent : {$lt : minStudent}},
            {noStudent : {$gt : maxStudent}}
        ]
    }
    // B3: Xu ly du lieu
    try {
        const result = await courseModel.find(condition).skip(1).sort({title:'desc'});

        return res.status(200).json({
            message: "Lay danh sach course thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await courseModel.findById(courseid);

        if(result) {
            return res.status(200).json({
                message: "Lay thong tin course thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;
    const {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    if(reqTitle === "") {
        return res.status(400).json({
            message: "Title khong hop le"
        })
    }

    if(reqStudent < 0) {
        return res.status(400).json({
            message: "So hoc sinh khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateCourse = {};
        if(reqTitle) {
            newUpdateCourse.title = reqTitle
        }
        if(reqDescription) {
            newUpdateCourse.description = reqDescription
        }
        if(reqStudent) {
            newUpdateCourse.noStudent = reqStudent
        }

        const result = await courseModel.findByIdAndUpdate(courseid, newUpdateCourse);

        if(result) {
            return res.status(200).json({
                message: "Update thong tin course thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}   

const deleteCourseById = async (req, res) => {
    // B1: Thu thap du lieu
    const courseid = req.params.courseid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            message: "Course ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await courseModel.findByIdAndRemove(courseid);

        if(result) {
            return res.status(200).json({
                message: "Xoa thong tin course thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin course"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

module.exports = {
    createCourse,
    getAllCourses,
    getCourseById,
    updateCourseById,
    deleteCourseById
}